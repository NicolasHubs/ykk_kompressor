FROM openjdk:11.0-jre
RUN apt-get -q update && apt-get -q install dumb-init
RUN mkdir app/
#EXPOSE 8080
ENTRYPOINT ["dumb-init", "--"]
CMD java -jar app/ykk_kompressor-*.jar
COPY target/ykk_kompressor-*.jar /app/ykk_kompressor-*.jar
