# YKK_kompressor
 Java version : 11.0.2<br/>
 Maven version : 3.6.3
 
 The goal of this project was to develop an application and apply, during its development, good development practices using several testing techniques introduced in class and set up a Continuous Integration system.
 
 A jar file of the application is available in the ``jar`` folder. It can be executed using the command : 
 ```
 java -jar jar/ykk_kompressor-release-0.1.4.jar
 ``` 
 
 ## How to use dockerized application 
 
 To easily run the application we decided to use the GitLab Container Registry system, which allows a GitLab project to store 
 docker images.
 In order to use the application, you just have to pull and run the docker image.
 To do this, you must ensure that you are successfully added to the project in order to access the GitLab Container Registry. 
 
 The very first time, you must connect your **GITLAB** account to the Container Registry 
 (a github account will not work) by using this command :  
 ```
 docker login registry.gitlab.com/nicolashubs/ykk_kompressor
 ```
  
 Next, you will need to pull the latest version of the project : 
 ```
 docker pull registry.gitlab.com/nicolashubs/ykk_kompressor:latest
 ```
 
 Finally, whenever you want to launch the application, you just have to run the following command : 
 ```
 docker run -it -v /home:/home registry.gitlab.com/nicolashubs/ykk_kompressor:latest
 ```
 
 Explanation of the arguments -i, -t and -v : <br>
 Since the project is an interactive process (like a shell), we use -i and -t together in order to have a functional 
 keyboard input and -v to allow docker to have access to the home folder (/home) to access the file path given in the console interface.
 
  When using the console interface, to avoid file rights issues, 
  it is better to use absolute path from ``/home``, for example :  
 > /home/user/files/file.txt
 
 If you want to recover a file that is in the running docker environment to your local file system, 
 you can run the following command :  
 ```
 docker cp $(docker ps | grep "registry.gitlab.com/nicolashubs/ykk_kompressor:latest" | cut -d' ' -f1):/results/filename .
 ```
 
 ## How to use the console interface 
  
 The application allows compression and decompression using two different algorithm available in the console interface : 

 - Huffman coding 
 - Lempel-Ziv-Welch algorithm

 ![alt text](img/interface_1.png?raw=true "Interface input choice")
 
 In this interface you can choose to : 
 - Give the path to a file to compress or decompress
 - Take a text from the keyboard
 - Exit the application
 
 ![alt text](img/interface_2.png?raw=true "Interface type choice")
 
 In this interface you can choose to : 
 - Compress a file
 - Decompress a file
 - Exit the application
 
 ![alt text](img/interface_3.png?raw=true "Interface algorithm choice")
 
 In this interface you can choose the algorithm to use :
 - Huffman coding 
 - Lempel-Ziv-Welch algorithm
 - Exit the application
 
 ![alt text](img/interface_result.png?raw=true "Interface result")
 
 Finally, the interface prints information about the compression/decompression and the destination file. 
 
 ## Continuous integration
 
 Having already learned how to use Jenkins during our sandwich course, we decided to choose GitLab CI for the continuous integration
 in order to discover how it works and how to use it. 
 
 Instead of using the free VMs provided by GitLab for the compilation of the project, our continuous integration chain is configured to use our own computer 
 (the runners).
 This allows us to compile faster and to understand/debug more easily the continuous integration issues that 
 we may face.
 
 It will execute a set of tests (listed later), 
 generate a new version of the application as a docker image that will be stored in the GitLab registry and
 finally send an email with a .tar.gz file containing all the results of the previous tests under .html format.
  
 We did not set any results analysis system (like verifying the coverage value, the number of errors raised by PMD…) 
 to authorize or not the generation of the docker image because it would have 
 been to complex to set up. 
 
 We decided to trigger the continuous integration chain not for each commit but 
 when we are committing changes with a name matching "release-*" or when 
 it was triggered manually on GitLab (CI/CD->Schedules->Generate Release play button).
 
 ![alt text](img/manual_generate_release.png?raw=true "Manual release")

 This allows us to do small changes (readme.md for exemple) 
 without starting every time a whole cycle of continuous integration on our computer.
 
 #### Runner configuration
 
 GitLab Runner is the open source project used to execute jobs and return results to GitLab.
 It is used in conjunction with GitLab CI (the open source continuous integration service included in GitLab) which coordinates jobs.
 
 We decided to use it because the free VMs provided by GitLab are slow and difficult to configure (version of java, maven...)
 
 Our two machines are currently configured as runner to replace the GitLab VM. 
 The runners are available when our computer are on.
 
 It is possible to add a new runner by following these instructions (note that you will then need to configure the java and maven version of your environment in order to compile the project) :
 https://docs.gitlab.com/runner/install/linux-manually.html
 
 In the step "5. Register the Runner", the GitLab instance URL and token values are available following this link : 
 https://gitlab.com/NicolasHubs/ykk_kompressor/-/settings/ci_cd
 then by clicking in Runner -> Set up a specific Runner manually.
 
 The type of the runner executor will be a shell, it is a simple executor that allows to execute builds 
 on the machine on which the runner is installed.
 
 The required java version for the compilation is : 11.0.2<br/>
 Tutorial : <br/>
 (You will have to change every occurrences of **jdk-11** with **jdk-11.0.2** in this tutorial)<br/>
 https://www.javahelps.com/2017/09/install-oracle-jdk-9-on-linux.html<br/>
 Tar of java 11.0.2 available with the command : 
 ```
 curl -O https://download.java.net/java/GA/jdk11/9/GPL/openjdk-11.0.2_linux-x64_bin.tar.gz
 ```
 
 The required Maven version for the compilation is : 3.6.3<br/>
 Tutorial : <br/>
 https://linuxize.com/post/how-to-install-apache-maven-on-ubuntu-18-04/
 
 If you want to allow your runner to execute several task concurrently when possible, 
 you should change the configuration of your gitlab-runner which can be found : 
 
   - /etc/gitlab-runner/config.toml on *nix system when Gitlab Runner is un as root
    
   - ~/.gitlab-runner/config.toml on *nix system when Gitlab Runner is not run as root
    
   - ./config.toml on all the other systems
   
 And then change the value *"concurent"*, for example :
 ```
 concurrent = 6
 ```
 Just to be sure that changes have been taken into account, restart the runner : 
 ```
 gitlab-runner restart
 ```
 
 #### Steps
 ![alt text](img/ci_steps.png?raw=true "ci steps")
 
 Our continuous integration cycle is divided into 8 jobs divided into 5 stages (it allows us to define the execution order of the jobs).
 1. **Build :** This step validate that the project is correct by executing all the plugin that are bounded to the **validate phase** (in our case the validate phase will do nothing, it is a specific maven step) and then it will execute the **compile phase** which will compile the source code
 2. **Test :** Test the compiled source code using a suitable unit testing framework, in our case junit 5
 3. **Package :** This step will take the compiled code and package it in its distributable format, such as a JAR.
 4. **Parallel_steps :** This step is used to generate reports with various tools / dockerize the application / launch jqf fuzz tests / update online sonarcloud coverage
 5. **Send_email :** This step is used to send an email with a link to the online sonarcloud interface and a .tar.gz file that contains a website (you just need to open the index.html) generated which is structured especially for the project. In it, you will be able to access all the generated reports, project informations, developers contact, dependency list and other. (The email recipient value is customizable in https://gitlab.com/NicolasHubs/ykk_kompressor/-/settings/ci_cd then variables and GITLAB_USER_EMAIL).
 
 SonarCloud interfaces : 
 ![alt text](img/sonarcloud_projects.png?raw=true "sonarcloud projects")
 
 ![alt text](img/sonarcloud_qualitygate.png?raw=true "sonarcloud quality gate")
 
 ![alt text](img/sonarcloud_issues.png?raw=true "sonarcloud issues")
 
 Website generated : 
 ![alt text](img/project_information.png?raw=true "project information")
 
 #### Report generated
 
 Project reports : 
 ![alt text](img/project_reports.png?raw=true "project reports")
 
 PMD
 ![alt text](img/report_PMD.png?raw=true "PMD")
 
 CPD
 ![alt text](img/report_CPD.png?raw=true "CPD")
 
 FindBugs
 ![alt text](img/report_findbugs.png?raw=true "findbugs")
 
 Checkstyle
 ![alt text](img/report_checkstyle.png?raw=true "checkstyle")
 
 PITest
 ![alt text](img/report_pitest.png?raw=true "pitest")
 
 Surefire
 ![alt text](img/report_surefire.png?raw=true "surefire")
 
 JaCoCo
 ![alt text](img/report_jacoco.png?raw=true "jacoco")
 
 Dependency check
 ![alt text](img/report_dependencycheck.png?raw=true "dependency check")
 
 Due to a lack of time we were unable to implement Cucumber and JQF tests, but the necessary integration steps are configured correctly.
 Indeed, some tests to try these plugins are present in the project and are functional but they are not related to our algorithms.
 JQF generates a report which is in the directory site/jqf-fuzz send in attachment.
 
 ## Test techniques
 
 ### Static testing/analysis
 
  - **PMD (+CPD)**<br/>
 
 *PMD* (+CPD) is a framework which analyze Java source code.
 It contains many rules that ensure the quality of the code : 
 unnecessary code, to complexes nesting... 
 It also makes possible to get the analysis results through a report.

  - **FindBugs**<br/>
 
 *FindBugs* is a free software for static analysis of Java bytecode. 
 His goal is to find bugs in Java programs by identifying recognized patterns as bugs.
 
  - **Checkstyle**<br/> 
 
 *Checkstyle* is a tool which check the code used in software development, 
 it allows to ensure a well-defined level of source code quality. 
 The verification mainly focus on form and do not allow to say if a program is correct or not.
 
 ### Mutation testing/analysis
 
  - **PITest**<br/>
  
 *PITest* is a system of mutation testing which will execute our unit tests 
 with automatically modified version of our code. When the code of the application changes, 
 it must produce different result and the tests must failed. 
 If a unit test does not fail it implies that there is a problem with the logic of the test.
  
 ### Fuzz testing 
 
  - **JQF + Zest**<br/> 
  
 The idea in fuzz testing is to inject random generated values in the program. 
 If the program fail, then there are defect to be corrected.
 To do fuzz testing we choose *JQF* which is a Java platform that uses several fuzzing algorithms such as :  
  - **AFL** - Binary fuzzing 
  - **Zest** - Semantic fuzzing 
  - **PerfFuzz** - Complexity fuzzing 
  - **RLCheck** - Reinforcement learning 
    
 ### Other
 
  - **Junit**<br/>
  
 *Junit* is a framework of unit testing which aim to create test 
 that will only test a single functionality, the most unitarian possible, with the smallest possible granularity. 
 This implies that an unit test can only be break in the case where the tested function has been modified. 
 In other words, a change in a class that does not concern the unit test should not affect this unit test.

  - **Cucumber**<br/>
  
 *Cucumber* is a tool which allows to make automated tests. 
 These test are written in the BDD (Behavior-Driven Development) style. 
 We find the story, scenarios and stops (given, when, then). They describe the functionality of the 
 software from the user point of view.
  
  - **Dependency check**<br/>

 *Dependency Check* is a tool which can list and check automatically all the dependencies in the application
 if a CVE (Common Vulnerabilities and Exposures) has been published for the version used.
 The database used by Dependency Check is the NIST's NVD Data Feeds.
 
  - **SonarCloud**<br/>
  
  *SonarCloud* is a code quality measurement tool which allows us to make report on :
 
  1. **code duplication** identification
  2. **documentation level** measurement
  3. compliance with **programming rules**
  4. detection of **potential bugs**
  5. evaluation of **code coverage** by unit tests
  6. **complexity distribution** analysis - analysis of the design and **architecture of the application**
  
  - **JaCoCo**<br/>
  
 *JaCoCo* or Java Code Coverage, is a code quality measurement tool like SonarCloud.
 
 ## Problems found
 
 The longest and most complex part was to create a working Continuous Integration 
 system with GitLab CI. 
 
 There have been many syntax issues, which is poorly documented when we want specific steps, 
 for example the triggering of a job when certain conditions are met, the configuration of a very specific environment on which the gitlab job will be executed
 (good versions of Java + maven + ssh command + git, docker in docker...).
 
 It took several tries/commits before having a functional GitLab CI, with all steps going in the
 correct order and only when the commit's name starts with "release".
 
 Many commits were not relevant (small programming changes), this is why they will not be visible in the commits tree so as not to have one too polluted.
 All of these commits have been merged into one (interactive rebase->squash commits->force push) : cf3ffcf5a8932225e42d755b26d3f38480d7ac66,
 then the gitlab-ci.yml file has been improved over time.
 
 It was long and tedious because at this time we were using the free VMs of GitLab, 
 which were not accessible in ssh (so it was complicated to debug when a problem appeared)
 and which took time to start the execution of the continuous integration cycle to finally realize that git was not installed for a specific stage of the build
 , that the verification of the commit name had to be checked at each step or that the docker environment used for a specific step could not execute an ssh command and mvn command.
