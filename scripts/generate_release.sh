#@IgnoreInspection BashAddShebang
if [[ $(git diff --name-only | grep 'pom.xml') ]]; then
    echo "Please commit your changes concerning the 'pom.xml' file before releasing a new version."
else
    pom_version=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout | tail -1)

    pom_number=$(echo ${pom_version} | sed 's/[^0-9]*//g')
    if [[ ! -z pom_version ]]
    then
        echo "Current pom version -> $pom_version"

        new_version=$(($(echo "$pom_version" | grep -Eo '[0-9]+$') + 1))
        new_version="$(echo "$pom_version" | sed 's/[0-9]\+$//')$new_version"

        echo "New version -> $new_version"
        mvn versions:set -DnewVersion=${new_version}
        git add pom.xml
        git commit -m "${new_version}"
        git push
        #git tag ${tag_version}
        #git push --tags
    fi
fi
