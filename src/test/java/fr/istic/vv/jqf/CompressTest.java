package fr.istic.vv.jqf;

import edu.berkeley.cs.jqf.fuzz.Fuzz;
import edu.berkeley.cs.jqf.fuzz.JQF;
import org.junit.Assume;
import org.junit.runner.RunWith;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import com.pholser.junit.quickcheck.generator.Size;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;

@RunWith(JQF.class)
@SuppressWarnings("squid:S2187")
public class CompressTest {
    @Fuzz
    public void bzip2(byte @Size(min=100, max=100)[] bytes){
        OutputStream o = new ByteArrayOutputStream();
        try {
            BZip2CompressorOutputStream bo = new BZip2CompressorOutputStream(o);
            bo.write(bytes);
            bo.finish();
        } catch (IOException e){
            Assume.assumeNoException(e);
        }
    }
}
