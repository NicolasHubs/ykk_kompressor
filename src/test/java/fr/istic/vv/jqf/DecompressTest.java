package fr.istic.vv.jqf;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.DeflaterInputStream;

import edu.berkeley.cs.jqf.fuzz.Fuzz;
import edu.berkeley.cs.jqf.fuzz.JQF;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.junit.runner.RunWith;

@RunWith(JQF.class)
@SuppressWarnings("squid:S2187")
public class DecompressTest {

    @Fuzz
    public void bzip2(InputStream in) {
        byte[] destBuffer = new byte[1024];
        try {
            new BZip2CompressorInputStream(in)
                    .read(destBuffer, 0, destBuffer.length);
        } catch (IOException e) {
            // Ignore
        }

    }

    @Fuzz
    public void deflate(InputStream in) {
        byte[] destBuffer = new byte[1024];
        try {
            new DeflaterInputStream(in)
                    .read(destBuffer, 0, destBuffer.length);
        } catch (IOException e) {
            // Ignore
        }

    }


}