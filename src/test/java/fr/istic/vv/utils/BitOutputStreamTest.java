package fr.istic.vv.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import java.io.OutputStream;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

class BitOutputStreamTest {

    @Mock
    private OutputStream mockOut;

    private BitOutputStream bitOutputStreamUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        bitOutputStreamUnderTest = new BitOutputStream(mockOut);
    }

    @Test
    void testWrite() throws Exception {
        // Setup

        // Run the test
        bitOutputStreamUnderTest.write(0);
        bitOutputStreamUnderTest.write(0);
        bitOutputStreamUnderTest.write(1);
        bitOutputStreamUnderTest.write(0);
        bitOutputStreamUnderTest.write(1);
        bitOutputStreamUnderTest.write(0);
        bitOutputStreamUnderTest.write(0);
        bitOutputStreamUnderTest.write(1);

        Mockito.verify(mockOut, times(1)).write(41);
        // Verify the results
    }

    @Test
    void testWrite_ThrowsIllegalArgumentException() {
        // Setup

        // Run the test
        assertThrows(IllegalArgumentException.class, () -> bitOutputStreamUnderTest.write(-1));
    }

    @Test
    void testClose() throws Exception {
        // Setup

        bitOutputStreamUnderTest.write(1);
        bitOutputStreamUnderTest.write(0);

        // Run the test
        bitOutputStreamUnderTest.close();
        Mockito.verify(mockOut, times(1)).write(128);
        Mockito.verify(mockOut, times(1)).close();

        // Verify the results
    }
}
