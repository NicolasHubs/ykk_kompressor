package fr.istic.vv.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class BitInputStreamTest {

    private BitInputStream bitInputStreamUnderTest;

    @BeforeEach
    void setUp() {
        bitInputStreamUnderTest = new BitInputStream(new ByteArrayInputStream("content".getBytes()));
    }

    @Test
    void testRead() throws Exception {
        // Setup

        // Run the test
        final int result = bitInputStreamUnderTest.read();

        // Verify the results
        assertEquals(0, result);
    }

    @Test
    void testReadEOF() throws Exception {
        // Setup
        bitInputStreamUnderTest.close();

        // Run the test
        final int result = bitInputStreamUnderTest.read();

        // Verify the results
        assertEquals(-1, result);

    }

    @Test
    void testReadNoEof() throws Exception {
        // Setup

        // Run the test
        final int result = bitInputStreamUnderTest.readNoEof();

        // Verify the results
        assertEquals(0, result);
    }

    @Test
    void testReadNoEof_ThrowsIOException() {
        // Setup
        bitInputStreamUnderTest = new BitInputStream(new ByteArrayInputStream("".getBytes()));

        // Run the test
        assertThrows(IOException.class, () -> bitInputStreamUnderTest.readNoEof());
    }

    @Test
    @SuppressWarnings("squid:S2699")
    void testClose() throws Exception {
        // Setup

        // Run the test
        bitInputStreamUnderTest.close();

        // Verify the results
    }
}
