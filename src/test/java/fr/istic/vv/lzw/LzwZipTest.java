package fr.istic.vv.lzw;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

class LzwZipTest {

    private LzwZip lzwZip;

    @BeforeEach
    void setUp() {
        lzwZip = new LzwZip();
    }

    @Test
    void compressTest() {
        // Setup
        InputStream inputStream = new ByteArrayInputStream("AIAIIaIJZOjas,S AA, SASIASI".getBytes());
        List<Integer> expectedResult = Arrays.asList(65, 73, 256, 73, 97, 73, 74, 90, 79, 106, 97, 115, 44, 83, 32, 65, 65, 44, 32, 83, 65, 83, 257, 277);
        lzwZip.configure(inputStream);

        // Run the test
        List<Integer> result = lzwZip.compress(Charset.defaultCharset());

        // Verify the results
        assertNotNull(result, "Le résultat de compress() ne doit pas être null");
        assertEquals(expectedResult.size(), result.size(), "Le résultat de compress() ne possède pas le même nombre de valeurs que le résultat attendu");

        for (int i = 0; i < result.size(); i++) {
            assertEquals(expectedResult.get(i), result.get(i));
        }
    }

    @Test
    void compressEmptyTest() {
        // Setup
        InputStream inputStream = new ByteArrayInputStream("".getBytes());
        List<Integer> expectedResult = Collections.emptyList();
        lzwZip.configure(inputStream);

        // Run the test
        List<Integer> result = lzwZip.compress(Charset.defaultCharset());

        // Verify the results
        assertNotNull(result, "Le résultat de compress() ne doit pas être null");
        assertEquals(expectedResult.size(), result.size(), "Le résultat de compress() ne possède pas le même nombre de valeurs que le résultat attendu");
    }


    @Test
    void compressNullTest() {
        // Setup
        List<Integer> expectedResult = Collections.emptyList();
        lzwZip.configure(null);

        // Run the test
        List<Integer> result = lzwZip.compress(Charset.defaultCharset());

        // Verify the results
        assertNotNull(result, "Le résultat de compress() ne doit pas être null");
        assertEquals(expectedResult.size(), result.size(), "Le résultat de compress() ne possède pas le même nombre de valeurs que le résultat attendu");
    }

    @Test
    void compressExecuteFullWorkflowTest() {
        // Setup
        InputStream inputStream = new ByteArrayInputStream("coucoublazbkazie".getBytes());
        List<Integer> expectedResult = Arrays.asList(99, 111, 117, 256, 117, 98, 108, 97, 122, 98, 107, 263, 105, 101);
        lzwZip.configure(inputStream);

        // Run the test
        lzwZip.execute();
        lzwZip.getResult();

        // Verify the results
        List<Integer> result = lzwZip.getCompressedValue();
        assertNotNull(result, "Le résultat de getCompressedValue() ne doit pas être null");
        assertEquals(expectedResult.size(), result.size(), "Le résultat de compress() ne possède pas le même nombre de valeurs que le résultat attendu");

        for (int i = 0; i < result.size(); i++) {
            assertEquals(expectedResult.get(i), result.get(i));
        }
    }
}