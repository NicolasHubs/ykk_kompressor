package fr.istic.vv.lzw;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class LzwUnzipTest {

    private LzwUnzip lzwUnzip;

    @BeforeEach
    void setUp() {
        lzwUnzip = new LzwUnzip();
    }

    @Test
    void decompressTest() throws IOException {
        // Setup
        InputStream inputStream = new ByteArrayInputStream("BABĀZāAZBĄăĂąSBS".getBytes());
        String expectedResult = "BABBAZABAZBZABAZBBABASBS";
        lzwUnzip.configure(inputStream);

        // Run the test
        String result = lzwUnzip.decompress(Charset.defaultCharset());

        // Verify the results
        assertNotNull(result, "Le résultat de decompress() ne doit pas être null");
        assertEquals(expectedResult.length(), result.length(), "Le résultat de decompress() ne possède pas le même nombre de valeurs que le résultat attendu");
        assertEquals(expectedResult, result, "Le résultat de decompress() aurait du etre identique au résultat attendu");
    }

    @Test
    void decompressEmptyTest() throws IOException {
        // Setup
        InputStream inputStream = new ByteArrayInputStream("".getBytes());
        String expectedResult = "";
        lzwUnzip.configure(inputStream);

        // Run the test
        String result = lzwUnzip.decompress(Charset.defaultCharset());

        // Verify the results
        assertNotNull(result, "Le résultat de decompress() ne doit pas être null");
        assertEquals(expectedResult.length(), result.length(), "Le résultat de decompress() ne possède pas le même nombre de valeurs que le résultat attendu");
        assertEquals(expectedResult, result, "Le résultat de decompress() aurait du etre identique au résultat attendu");
    }

    @Test
    void decompressNullTest() throws IOException {
        // Setup
        String expectedResult = "";
        lzwUnzip.configure(null);

        // Run the test
        String result = lzwUnzip.decompress(Charset.defaultCharset());

        // Verify the results
        assertNotNull(result, "Le résultat de decompress() ne doit pas être null");
        assertEquals(expectedResult.length(), result.length(), "Le résultat de decompress() ne possède pas le même nombre de valeurs que le résultat attendu");
        assertEquals(expectedResult, result, "Le résultat de decompress() aurait du etre identique au résultat attendu");
    }

    @Test
    void decompressKEqualsDictSizeTest() throws IOException {
        // Setup
        InputStream inputStream = new ByteArrayInputStream("AĀ".getBytes());
        String expectedResult = "AAA";
        lzwUnzip.configure(inputStream);

        // Run the test
        String result = lzwUnzip.decompress(Charset.defaultCharset());

        // Verify the results
        assertNotNull(result, "Le résultat de decompress() ne doit pas être null");
        assertEquals(expectedResult.length(), result.length(), "Le résultat de decompress() ne possède pas le même nombre de valeurs que le résultat attendu");
        assertEquals(expectedResult, result, "Le résultat de decompress() aurait du etre identique au résultat attendu");
    }

    @Test
    void decompressTest_ThrowsIOException() throws IOException {
        // Setup
        InputStream inputStream = new ByteArrayInputStream("AĀi".getBytes());
        lzwUnzip.configure(inputStream);

        // Run the test + Verify the results
        assertThrows(IllegalArgumentException.class, () -> lzwUnzip.decompress(StandardCharsets.UTF_16));
    }

    @Test
    void getIntegerListTest() throws IOException {
        // Setup
        InputStream inputStream = new ByteArrayInputStream("BABĀZāAZBĄăĂąSBS".getBytes());
        List<Integer> expectedResult = Arrays.asList(66, 65, 66, 256, 90, 257, 65, 90, 66, 260, 259, 258, 261, 83, 66, 83);

        // Run the test
        List<Integer> result = lzwUnzip.getIntegerList(inputStream, Charset.defaultCharset());

        // Verify the results
        assertNotNull(result, "Le résultat de getIntegerList() ne doit pas être null");
        assertEquals(expectedResult.size(), result.size(), "Le résultat de getIntegerList() ne possède pas le même nombre de valeurs que le résultat attendu");

        for (int i = 0; i < result.size(); i++) {
            assertEquals(expectedResult.get(i), result.get(i));
        }
    }

    @Test
    void getIntegerListEmptyTest() throws IOException {
        // Setup
        InputStream inputStream = new ByteArrayInputStream("".getBytes());
        List<Integer> expectedResult = Collections.emptyList();

        // Run the test
        List<Integer> result = lzwUnzip.getIntegerList(inputStream, Charset.defaultCharset());

        // Verify the results
        assertNotNull(result, "Le résultat de getIntegerList() ne doit pas être null");
        assertEquals(expectedResult.size(), result.size(), "Le résultat de getIntegerList() ne possède pas le même nombre de valeurs que le résultat attendu");
    }

    @Test
    void compressExecuteFullWorkflowTest() throws IOException {
        // Setup
        InputStream inputStream = new ByteArrayInputStream("couĀublazbkćie".getBytes());
        byte[] expectedResult = {99, 111, 117, 99, 111, 117, 98, 108, 97, 122, 98, 107, 97, 122, 105, 101};
        lzwUnzip.configure(inputStream);

        // Run the test
        lzwUnzip.execute();

        InputStream is = lzwUnzip.getResult();
        assertNotNull(is, "L'inputstream ne doit pas être null");

        byte[] zip = is.readAllBytes();

        // Verify the results
        //assertEquals(expectedResult.length, zip.length, "Le résultat de compress() ne possède pas le même nombre de valeurs que le résultat attendu");

        for (int i = 0; i < zip.length; i++) {
            assertEquals(expectedResult[i], zip[i]);
        }
    }
}