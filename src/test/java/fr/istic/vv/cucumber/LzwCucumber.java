package fr.istic.vv.cucumber;

import cucumber.api.java.en.Given;
import fr.istic.vv.lzw.LzwUnzip;
import fr.istic.vv.lzw.LzwZip;

public class LzwCucumber {
    private LzwZip lzwZip;
    private LzwUnzip lzwUnzip;


    @Given("^my LzwZip exists$")
    public void my_lzwZip_exists() {
        lzwZip = new LzwZip();
    }

    @Given("^my LzwUnzip exists$")
    public void my_lzwUnzip_exists() {
        lzwUnzip = new LzwUnzip();
    }
}
