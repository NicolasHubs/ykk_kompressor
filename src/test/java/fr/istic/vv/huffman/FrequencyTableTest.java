package fr.istic.vv.huffman;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

class FrequencyTableTest {

    private FrequencyTable frequencyTableUnderTest;

    @BeforeEach
    void setUp() {
        frequencyTableUnderTest = new FrequencyTable(new int[]{1,2,3});
    }

    @Test
    void testConstructor_trows_IllegalArgumentException_notEnoughSymbols(){
        //setup
        int[] freqs = {2};

        //verify the results
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> new FrequencyTable(freqs));
        assertEquals("At least 2 symbols needed", ex.getMessage());
    }

    @Test
    void testConstructor_throws_IllegalArgumentException_negativeFrequencies(){
        //setup
        int[] freqs = {-1, 2, 3};

        //verify the results
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> new FrequencyTable(freqs));
        assertEquals("Negative frequency", ex.getMessage() );

    }

    @Test
    void testConstructor_withSizeOf2(){
        //setup
        int[] freqs = {1,2};

        //verify the results
        assertDoesNotThrow(() -> new FrequencyTable(freqs));
    }

    @Test
    void testGetSymbolLimit() {
        // Setup
        int size = 3;

        // Run the test
        final int result = frequencyTableUnderTest.getSymbolLimit();

        // Verify the results
        assertEquals(size, result);
    }

    @Test
    void testGet() {
        //setup
        int expected = 3;

        // Run the test
        final int result = frequencyTableUnderTest.get(2);

        // Verify the results
        assertEquals(expected, result);
    }

    @Test
    void testGet_throws_IllegalArgumentExeption_outOfRange() {
        //Setup
        int pos = frequencyTableUnderTest.getSymbolLimit() + 1;

        // Verify the results
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> frequencyTableUnderTest.get(pos));
        assertEquals("Symbol out of range", ex.getMessage());
    }

    @Test
    void testSet_throws_IllegalArgumentExeption_outOfRange() {
        //Setup
        int pos = frequencyTableUnderTest.getSymbolLimit() + 1;

        // Verify the results
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> frequencyTableUnderTest.set(pos, 2));
        assertEquals("Symbol out of range", ex.getMessage());
    }

    @Test
    void testSet_throws_IllegalArgumentExeption_negativeFrequency() {
        //Setup
        int pos = 0;
        int value = -1;

        // Verify the results
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> frequencyTableUnderTest.set(pos, value));
        assertEquals("Negative frequency", ex.getMessage());
    }

    @Test
    void testSet_0Frequency() {
        //Setup
        int pos = 0;
        int value = 0;

        // Verify the results
        assertDoesNotThrow(() -> frequencyTableUnderTest.set(pos, value));
    }

    @Test
    void testIncrement_throws_IllegalArgumentExeption_outOfRange() {
        //Setup
        int pos = frequencyTableUnderTest.getSymbolLimit() + 1;

        // Verify the results
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> frequencyTableUnderTest.increment(pos));
        assertEquals("Symbol out of range", ex.getMessage());
    }

    @Test
    void testIncrement_throws_IllegalStateExeption_IntegerMaxRange() {
        //Setup
        frequencyTableUnderTest = new FrequencyTable(new int[]{1, 2, Integer.MAX_VALUE});

        int pos = 2;

        // Verify the results
        IllegalStateException ex = assertThrows(IllegalStateException.class, () -> frequencyTableUnderTest.increment(pos));
        assertEquals("Maximum frequency reached", ex.getMessage());
    }



    @Test
    @SuppressWarnings("squid:S2699")
    void testIncrement() {
        // Setup
        int pos = 0;
        int pos2 = 1;
        int currentFrequency = frequencyTableUnderTest.get(pos);
        int currentFrequency2 = frequencyTableUnderTest.get(pos);

        // Run the test
        frequencyTableUnderTest.increment(pos);

        // Verify the results
        assertEquals(currentFrequency + 1, frequencyTableUnderTest.get(pos));
        assertEquals(currentFrequency2 + 1, frequencyTableUnderTest.get(pos2));

    }

    @Test
    void testToString() {
        // Setup
        String result = String.format("%d\t%d%n"+"%d\t%d%n"+"%d\t%d%n", 0,1,1,2,2,3);

        // Verify the results
        assertEquals(result, frequencyTableUnderTest.toString());
    }

    @Test
    void testBuildCodeTree() {
        // Setup
        CodeTree expectedCodeTree = new CodeTree(
                new InternalNode(
                        new InternalNode(
                                new Leaf(0),
                                new Leaf(1)),
                        new Leaf(2)),
                5);
        String expectedValue = expectedCodeTree.toString();

        // Run the test
        final CodeTree result = frequencyTableUnderTest.buildCodeTree();
        String resultValue = result.toString();

        // Verify the results
        assertEquals(expectedValue, resultValue);
    }

}
