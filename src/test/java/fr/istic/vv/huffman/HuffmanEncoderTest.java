package fr.istic.vv.huffman;

import fr.istic.vv.huffman.CodeTree;
import fr.istic.vv.huffman.HuffmanEncoder;
import fr.istic.vv.huffman.InternalNode;
import fr.istic.vv.huffman.Leaf;
import fr.istic.vv.utils.BitOutputStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import static org.junit.jupiter.api.Assertions.*;

class HuffmanEncoderTest {

    private HuffmanEncoder huffmanEncoderUnderTest;

    @BeforeEach
    void setUp() {
        huffmanEncoderUnderTest = new HuffmanEncoder(new BitOutputStream(OutputStream.nullOutputStream()));
    }

    @Test
    void testWrite() throws Exception {
        // Setup
        byte[] expectedResult = {73}; // 01001001
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        huffmanEncoderUnderTest = new HuffmanEncoder(new BitOutputStream(baos));
        huffmanEncoderUnderTest.setCodeTree(new CodeTree(
                new InternalNode(new Leaf(1), new Leaf(2)), 10
        ));

        // Run the test
        huffmanEncoderUnderTest.write(1); // 0
        huffmanEncoderUnderTest.write(2); // 1
        huffmanEncoderUnderTest.write(1); // 0
        huffmanEncoderUnderTest.write(1); // 0
        huffmanEncoderUnderTest.write(2); // 1
        huffmanEncoderUnderTest.write(1); // 0
        huffmanEncoderUnderTest.write(1); // 0
        huffmanEncoderUnderTest.write(2); // 1

        // Verify the results
        assertArrayEquals(baos.toByteArray(), expectedResult);
    }

    @Test
    void testConstructor_throws_NullPointerException() {
        // Setup

        //Run the test;
        assertThrows(NullPointerException.class, () -> huffmanEncoderUnderTest = new HuffmanEncoder(null));
    }


    @Test
    void testWrite_codeTree_throws_NullPointerException() {
        // Setup
        huffmanEncoderUnderTest.setCodeTree(null);
        int symbol = 1;

        // Run the test
        assertThrows(NullPointerException.class, () -> huffmanEncoderUnderTest.write(symbol));
    }
}
