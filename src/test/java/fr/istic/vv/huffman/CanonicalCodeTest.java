package fr.istic.vv.huffman;

import fr.istic.vv.huffman.CanonicalCode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.Null;

import static org.junit.jupiter.api.Assertions.*;

class CanonicalCodeTest {

    private CanonicalCode canonicalCodeUnderTest;

    @BeforeEach
    void setUp() {
        canonicalCodeUnderTest = new CanonicalCode(new CodeTree(new InternalNode(new InternalNode(new Leaf(1), new Leaf(2)), new Leaf(3)), 4), 4);
    }

    @Test
    void testConstructor_table_throws_nullPointerException() {
        //Run and verify test
        assertThrows(NullPointerException.class, () -> new CanonicalCode(null));
    }

    @Test
    void testConstructor_table_throws_illegalArgumentException_atLeast2Symb() {
        //Setup
        int[] values = new int[]{1};
        int[] values2 = new int[]{1,2};

        //Run and verify test
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class,
                () -> new CanonicalCode(values)
        );
        IllegalArgumentException ex2 = assertThrows(IllegalArgumentException.class,
                () -> new CanonicalCode(values2)
        );
        assertEquals("At least 2 symbols needed", ex.getMessage());
        assertNotEquals("At least 2 symbols needed", ex2.getMessage());
    }

    @Test
    void testConstructor_table_throws_nullPointerException_illegalvalue() {
        //Setup
        int[] values = new int[]{-1,2,3};

        //Run and verify test
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class,
                () -> new CanonicalCode(values)
        );
        assertEquals("Illegal code length", ex.getMessage());
    }

    @Test
    void testConstructor_codeTree_throws_illegalArgument_wrongLimit() {
        //Setup
        CodeTree tree = new CodeTree(new InternalNode(new Leaf(1), new Leaf(2)),3);

        int limit = 1;
        int limit2 = 2;

        //Run and verify test
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class,
                () -> new CanonicalCode(tree, limit)
        );
        IllegalArgumentException ex2 = assertThrows(IllegalArgumentException.class,
                () -> new CanonicalCode(tree, limit2)
        );
        assertEquals("At least 2 symbols needed", ex.getMessage());
        assertNotEquals("At least 2 symbols needed", ex2.getMessage());
    }

    @Test
    void testGetSymbolLimit() {
        // Setup
        int symbolLimit = 4;

        // Run the test
        final int result = canonicalCodeUnderTest.getSymbolLimit();

        // Verify the results
        assertEquals(symbolLimit, result);
    }

    @Test
    void testGetCodeLength() {
        // Setup
        int expectedLength = 1;

        // Run the test
        final int result = canonicalCodeUnderTest.getCodeLength(3);

        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class,
                () -> canonicalCodeUnderTest.getCodeLength(-1)
        );
        IllegalArgumentException ex2 = assertThrows(IllegalArgumentException.class,
                () -> canonicalCodeUnderTest.getCodeLength(4)
        );
        assertEquals("Symbol out of range", ex.getMessage());
        assertEquals("Symbol out of range", ex2.getMessage());

        // Verify the results
        assertEquals(expectedLength, result);
    }

    @Test
    @SuppressWarnings("squid:S2699")
    void testToCodeTree() {
        // Setup
        CodeTree expectedTree = new CodeTree(new InternalNode(new Leaf(3), new InternalNode(new Leaf(1), new Leaf(2))), 4);

        // Run the test
        final CodeTree result = canonicalCodeUnderTest.toCodeTree();

        // Verify the results
        assertEquals(expectedTree.toString(),result.toString());
    }
}
