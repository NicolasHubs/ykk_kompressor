package fr.istic.vv.huffman;

import fr.istic.vv.huffman.Leaf;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class LeafTest {

    @Test
    void leaf_should_be_created() {
        int symValue = 0;
        Leaf leafUnderTest = new Leaf(symValue);
        assertEquals(symValue, leafUnderTest.symbol);
    }

    @Test
    void leaf_should_throw_exception() {
        int symValue = -1;
        assertThrows(IllegalArgumentException.class, () -> new Leaf(symValue));
    }
}
