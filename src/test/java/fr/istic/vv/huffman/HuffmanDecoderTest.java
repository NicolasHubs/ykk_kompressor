package fr.istic.vv.huffman;

import fr.istic.vv.utils.BitInputStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class HuffmanDecoderTest {

    private HuffmanDecoder huffmanDecoderUnderTest;

    @BeforeEach
    void setUp() {
        huffmanDecoderUnderTest = new HuffmanDecoder(new BitInputStream(new ByteArrayInputStream("content".getBytes())));
    }

    @Test
    void testConstructor_throws_NullPointerException() {
        //Run the test;
        assertThrows(NullPointerException.class, () -> huffmanDecoderUnderTest = new HuffmanDecoder(null));
    }

    @Test
    void testRead() throws IOException {
        // Setup
        int expectedValue = 1;
        CodeTree codeTree = new CodeTree(new InternalNode(new Leaf(1), new Leaf(2)), 10);

        // Run the test
        huffmanDecoderUnderTest.setCodeTree(codeTree);
        final int result = huffmanDecoderUnderTest.read();

        // Verify the results
        assertEquals(expectedValue, result);
    }

    @Test
    void testRead_throws_NullPointerException_codeTreeNull() throws IOException {
        // Setup
        int expectedValue = 1;

        // Run the test
        NullPointerException ex = assertThrows(NullPointerException.class, () -> huffmanDecoderUnderTest.read());

        assertEquals("Code tree is null", ex.getMessage());
    }

    @Test
    void testRead_throws_NullPointerException() {
        // Setup
        huffmanDecoderUnderTest.setCodeTree(null);

        // Run the test
        assertThrows(NullPointerException.class, () -> huffmanDecoderUnderTest.read());
    }

    @Test
    @SuppressWarnings("squid:S2699")
    void testRead_throws_AssertionError() {
        // Setup

        // Run the test
        /*assertThrows(IOException.class, () -> {
            huffmanDecoderUnderTest.read();
        });*/
    }
}
