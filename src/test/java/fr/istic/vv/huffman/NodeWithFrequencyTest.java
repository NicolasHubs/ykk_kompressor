package fr.istic.vv.huffman;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

class NodeWithFrequencyTest {

    private Node node;
    private NodeWithFrequency nodeWithFrequencyUnderTest;

    @BeforeEach
    void setUp() {
        node = new Leaf(1);
        nodeWithFrequencyUnderTest = new NodeWithFrequency(node, 3, 2L);
    }

    @Test
    void testConstructeur(){
        // Setup
        int expectedLowSym = 3;
        Long expectedFreq = 2L;

        // Verify
        assertEquals(node, nodeWithFrequencyUnderTest.node);
        assertEquals(expectedLowSym, nodeWithFrequencyUnderTest.lowestSymbol);
        assertEquals(expectedFreq, nodeWithFrequencyUnderTest.frequency);
    }


    @Test
    void testCompareTo_biggerFrequency() {
        // Setup
        final NodeWithFrequency other = new NodeWithFrequency(new Leaf(2), 2, 1L);
        int expectedResult = 1;

        // Run the test
        final int result = nodeWithFrequencyUnderTest.compareTo(other);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    void testCompareTo_lowerFrequency() {
        // Setup
        final NodeWithFrequency other = new NodeWithFrequency(new Leaf(2), 2, 3L);
        int expectedResult = -1;

        // Run the test
        final int result = nodeWithFrequencyUnderTest.compareTo(other);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    void testCompareTo_EqualFrequency() {
        // Setup
        final NodeWithFrequency other = new NodeWithFrequency(new Leaf(2), 2, 2L);
        int expectedResult = 1;

        // Run the test
        final int result = nodeWithFrequencyUnderTest.compareTo(other);

        // Verify the results
        assertEquals(expectedResult, result);
    }
}
