package fr.istic.vv.huffman;

import fr.istic.vv.huffman.InternalNode;
import fr.istic.vv.huffman.Node;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;

import static org.mockito.MockitoAnnotations.initMocks;


@SuppressWarnings("squid:S2187")
class InternalNodeTest {

    @Mock
    private Node mockLeft;
    @Mock
    private Node mockRight;

    @BeforeEach
    void setUp() {
        initMocks(this);
        @SuppressWarnings("unused") InternalNode internalNodeUnderTest = new InternalNode(mockLeft, mockRight);
    }
}
