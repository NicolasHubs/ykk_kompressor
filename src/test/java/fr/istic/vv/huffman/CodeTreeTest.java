package fr.istic.vv.huffman;

import fr.istic.vv.huffman.CodeTree;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CodeTreeTest {

    @SuppressWarnings("unused")
    private CodeTree codeTreeUnderTest;

    @BeforeEach
    void setUp() {
        codeTreeUnderTest = new CodeTree(
                new InternalNode(
                        new InternalNode(new Leaf(1), new Leaf(2)),
                        new Leaf(4)), 5);
    }

    @Test
    void testConstructor(){
        //setup
        List<Integer> expectedResult1 = new ArrayList<>(Arrays.asList(0, 0));
        List<Integer> expectedResult2 = new ArrayList<>(Arrays.asList(0, 1));
        List<Integer> expectedResult3 = new ArrayList<>(Collections.singletonList(1));

        String expectedString = "Code 00: Symbol 1\n" +
                "Code 01: Symbol 2\n" +
                "Code 1: Symbol 3\n";

        //run test
        codeTreeUnderTest = new CodeTree(
                new InternalNode(
                        new InternalNode(new Leaf(1), new Leaf(2)),
                        new Leaf(3)), 4);

        //Verify the results
        assertEquals(expectedResult1, codeTreeUnderTest.getCode(1));
        assertEquals(expectedResult2, codeTreeUnderTest.getCode(2));
        assertEquals(expectedResult3, codeTreeUnderTest.getCode(3));
        assertEquals(expectedString, codeTreeUnderTest.toString());

    }

    @Test
    void testConstructor_throws_IllegalArgumentException_lessThan2Symbols()
    {
        //Run and verify test
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> new CodeTree(
                new InternalNode(new Leaf(1), new Leaf(2)), 1)
        );
        assertEquals("At least 2 symbols needed", ex.getMessage());
    }

    @Test
    void testConstructor_throws_IllegalArgumentException_exceedSymbolsLimit()
    {
        //Run and verify test
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> new CodeTree(
                new InternalNode(new Leaf(1), new Leaf(2)), 2)
        );
        IllegalArgumentException ex2 = assertThrows(IllegalArgumentException.class, () -> new CodeTree(
                new InternalNode(new Leaf(1), new Leaf(3)), 2)
        );
        assertEquals("Symbol exceeds symbol limit", ex.getMessage());
        assertEquals("Symbol exceeds symbol limit", ex2.getMessage());
    }

    @Test
    void testConstructor_throws_IllegalArgumentException_moreThanOneCode()
    {
        //Run and verify test
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> new CodeTree(
                new InternalNode(new Leaf(1), new Leaf(1)), 2)
        );
        assertEquals("Symbol has more than one code", ex.getMessage());
    }

    @Test
    void testConstructor_throws_assertionError_wrongClass()
    {
        //setup
        Node wrongNode = new Node(){};

        //Run and verify test
        AssertionError ex = assertThrows(AssertionError.class, () -> new CodeTree(
                new InternalNode(new Leaf(1), wrongNode), 2)
        );
        assertEquals("Illegal node type", ex.getMessage());
    }


    @Test
    void testGetCode_throws_IllegalArgumentException_illegalSymbol(){
        //setup
        int sym = -1;

        //Run and verify test
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> codeTreeUnderTest.getCode(sym)
        );
        assertEquals("Illegal symbol", ex.getMessage());
    }

    @Test
    void testGetCode_throws_IllegalArgumentException_noCode(){
        //setup
        int sym = 0;
        int sym2 = 3;

        //Run and verify test
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> codeTreeUnderTest.getCode(sym)
        );
        IllegalArgumentException ex2 = assertThrows(IllegalArgumentException.class, () -> codeTreeUnderTest.getCode(sym2)
        );
        assertEquals("No code for given symbol", ex.getMessage());
        assertEquals("No code for given symbol", ex2.getMessage());
    }

    @Test
    void testGetCode() {
        // Setup
        final List<Integer> expectedResult = Arrays.asList(0,1);

         // Run the test
         final List<Integer> result = codeTreeUnderTest.getCode(2);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    @SuppressWarnings("squid:S2699")
    void testToString() {
        // Setup
        String expectedString = "Code 00: Symbol 1\n" +
                "Code 01: Symbol 2\n" +
                "Code 1: Symbol 4\n";
        // Run the test
        final String result = codeTreeUnderTest.toString();

        // Verify the results
        assertEquals(expectedString, result);
    }
}
