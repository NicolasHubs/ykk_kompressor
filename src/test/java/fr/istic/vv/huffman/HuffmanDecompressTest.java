package fr.istic.vv.huffman;

import fr.istic.vv.huffman.HuffmanDecompress;
import fr.istic.vv.utils.BitInputStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class HuffmanDecompressTest {

    private HuffmanDecompress huffmanDecompressUnderTest;

    @BeforeEach
    void setUp() {
        huffmanDecompressUnderTest = new HuffmanDecompress();
    }

    @Test
    void testExecute() throws IOException {
        // Setup
        String contentToTest = "text";
        final InputStream inputStream = new ByteArrayInputStream(contentToTest.getBytes());
        HuffmanCompress huffmanCompress = new HuffmanCompress();
        huffmanCompress.configure(inputStream);
        huffmanCompress.execute();
        huffmanDecompressUnderTest.configure(huffmanCompress.getResult());

        // Run the test
        huffmanDecompressUnderTest.execute();

        // Verify the results
        //TODO Find a better way : not real unit test
        assertEquals(contentToTest, new String(huffmanDecompressUnderTest.getResult().readAllBytes()));
    }
}
