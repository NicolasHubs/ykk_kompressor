package fr.istic.vv.huffman;

import fr.istic.vv.huffman.HuffmanCompress;
import fr.istic.vv.huffman.HuffmanDecompress;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

class HuffmanCompressTest {

    @SuppressWarnings("unused")
    @Mock
    HuffmanCompress huffmanCompressMock;

    private HuffmanCompress huffmanCompressUnderTest;

    @BeforeEach
    void setUp() {
        huffmanCompressUnderTest = new HuffmanCompress();
    }

    @Test
    void testConfigure() {
        // Setup
        String contentToTest = "Text to compress";
        final InputStream inputStream = new ByteArrayInputStream(contentToTest.getBytes());

        // Run the test
        huffmanCompressUnderTest.configure(inputStream);

        // Verify the results
        assertEquals(contentToTest, huffmanCompressUnderTest.getContent(), "Content did not matches");
    }

    @Test
    void testExecute_with_no_error() throws Exception {
        // Setup
        String contentToTest = "text";
        //int[] i = {0,1,1,0,1,1,1,0};
        final InputStream inputStream = new ByteArrayInputStream(contentToTest.getBytes());
        huffmanCompressUnderTest.configure(inputStream);

        // Run the test
        huffmanCompressUnderTest.execute();
        HuffmanDecompress huffmanDecompress = new HuffmanDecompress();
        huffmanDecompress.configure(huffmanCompressUnderTest.getResult());
        huffmanDecompress.execute();

        // Verify the results
        assertEquals(new String(huffmanDecompress.getResult().readAllBytes()), contentToTest);
    }
}
