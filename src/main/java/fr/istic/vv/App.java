package fr.istic.vv;

import fr.istic.vv.huffman.HuffmanCompress;
import fr.istic.vv.huffman.HuffmanDecompress;
import fr.istic.vv.lzw.LzwUnzip;
import fr.istic.vv.lzw.LzwZip;
import fr.istic.vv.strategy.Strategy;
import fr.istic.vv.utils.FilesUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@SuppressWarnings({"squid:S106", "DuplicatedCode"})
public class App {
    private static String fileName;
    private static InputStream inputStream;
    private static boolean isCompression;
    private static Strategy selectedAlgo;
    private static String algoName;
    private static boolean isStandardInput;
    private static String extension;
    private static final String LZW_EXTENSION = "lzw";
    private static final String HUF_EXTENSION = "huf";
    private static final String HUF_NAME = "Huffman coding";
    private static final String LZW_NAME = "Lempel-Ziv-Welch algorithm";

    private static void initVars() {
        fileName = "";
        isCompression = false;
        isStandardInput = false;
        algoName = "";
        extension = "";
    }

    @SuppressWarnings("SameParameterValue")
    private static int getChoice(BufferedReader keyboard, int min, int max) {
        System.out.print("Enter your choice : ");

        int choice = -1;
        String entry = null;
        while (choice < min || choice > max) {
            if (entry != null) {
                System.out.println("Invalid value.");
                System.out.println("Expected a number >= " + min + " && <= " + max + ".");
            }
            try {
                entry = keyboard.readLine();
                choice = Integer.parseInt(entry);
            } catch (Exception e) {
                System.out.printf("'%s' is not an integer\r%n", entry);
                entry = null;
            }
        }
        return choice;
    }

    private static void printChoices(String... choices) {
        int i;
        for (i = 0; i < choices.length; i++) {
            System.out.println((i + 1) + ") " + choices[i]);
        }
        System.out.println((i + 1) + ") Quit");
    }

    private static void exitApp() {
        System.out.println("Exiting the application.");
        System.exit(0);
    }

    private static void showOptionsMenu(BufferedReader keyboard) throws IOException {
        printChoices("File path", "Standard input");

        int choice = getChoice(keyboard, 0, 3);
        switch (choice) {
            case 1:
                System.out.println("(It's recommended to enter an absolute path if you are in the dockerized app)");
                System.out.print("Enter the file path : ");
                fileName = keyboard.readLine();
                File file = new File(fileName);
                while (!file.exists() || !file.isFile()) {
                    if (!file.exists()) {
                        System.out.println("\"" + fileName + "\" does not exist.");
                    } else if (!file.isFile()) {
                        System.out.println("\"" + fileName + " is not a file.");
                    }

                    System.out.print("Enter the file path : ");
                    fileName = keyboard.readLine();
                    file = new File(fileName);
                }
                inputStream = new FileInputStream(fileName);
                break;
            case 2:
                System.out.println("You selected the standard input.");
                isStandardInput = true;
                break;
            case 3:
                exitApp();
            default:
        }
    }


    private static void showCompressionDecompression(BufferedReader keyboard) {
        printChoices("Compression", "Decompression");

        int choice = getChoice(keyboard, 0, 3);
        switch (choice) {
            case 1:
                isCompression = true;
                break;
            case 2:
                isCompression = false;
                break;
            case 3:
                exitApp();
            default:
        }
    }

    @SuppressWarnings("squid:S3776")
    private static void showAlgorithms(BufferedReader keyboard) {
        printChoices(HUF_NAME, LZW_NAME);

        Optional<String> fileExt;
        if (!isStandardInput) {
            fileExt = Optional.of(fileName).filter(f -> f.contains("."))
                    .map(f -> f.substring(fileName.lastIndexOf('.') + 1)).map(String::toLowerCase);

            if (fileExt.isPresent()) {
                String ext = fileExt.get();
                if (ext.equals(LZW_EXTENSION)) {
                    if (isCompression)
                        selectedAlgo = new LzwZip();
                    else
                        selectedAlgo = new LzwUnzip();
                    algoName = LZW_NAME;
                    extension = LZW_EXTENSION;
                    System.out.println("Extension detected, algo set up to '" + algoName + "' (." + extension + ")");
                    return;
                } else if (ext.equals(HUF_EXTENSION)) {
                    if (isCompression)
                        selectedAlgo = new HuffmanCompress();
                    else
                        selectedAlgo = new HuffmanDecompress();
                    algoName = HUF_NAME;
                    extension = HUF_EXTENSION;
                    System.out.println("Extension detected, algo set up to '" + algoName + "' (." + extension + ")");
                    return;
                }
            }
        }

        int choice = getChoice(keyboard, 0, 3);
        switch (choice) {
            case 1:
                if (isCompression)
                    selectedAlgo = new HuffmanCompress();
                else
                    selectedAlgo = new HuffmanDecompress();
                algoName = HUF_NAME;
                extension = HUF_EXTENSION;
                break;
            case 2:
                if (isCompression)
                    selectedAlgo = new LzwZip();
                else
                    selectedAlgo = new LzwUnzip();
                algoName = LZW_NAME;
                extension = LZW_EXTENSION;
                break;
            case 3:
                exitApp();
            default:
        }
    }

    @SuppressWarnings("squid:S899")
    private static void showExecutionStatus(BufferedReader keyboard) throws IOException {
        System.out.println(String.format("Starting %s %s with %s", algoName, (isCompression) ? "compression" : "decompression", (isStandardInput) ? " standard input" : "\"" + fileName + "\".."));

        if (isStandardInput) {
            String stopWord = "STOP_BUFFER";
            System.out.println("Type '" + stopWord + "' when you want to stop.");
            StringBuilder input = new StringBuilder();
            String line = keyboard.readLine();
            while (line != null && !line.contains(stopWord)) {
                input.append(line);
                line = keyboard.readLine();

                if (line != null && !line.contains(stopWord)) {
                    input.append("\n");
                } else if (line != null) {
                    String[] lineParsed = line.split(stopWord);
                    if (lineParsed.length >= 1) {
                        input.append("\n");
                        input.append(lineParsed[0]);
                    }
                }
            }

            inputStream = new ByteArrayInputStream(input.toString().getBytes());
            selectedAlgo.configure(inputStream);
            selectedAlgo.execute();
            System.out.println(String.format("%s ended successfully, result :", (isCompression) ? "Compression" : "Decompression"));
            if (isCompression && LZW_EXTENSION.equals(extension)) {
                for (Integer i : ((LzwZip) selectedAlgo).getCompressedValue())
                    System.out.print((char) i.intValue());
                System.out.println();
            } else {
                System.out.println(new String(selectedAlgo.getResult().readAllBytes(), StandardCharsets.UTF_8));
            }
        } else {
            fileName = new File(fileName).getName();

            if (isCompression) {
                fileName = "results/" + fileName + "." + extension;
            } else {
                fileName = "results/" + fileName.replaceFirst("[.][^.]+$", "");
            }

            selectedAlgo.configure(inputStream);
            selectedAlgo.execute();
            Path pathToFile = Paths.get(fileName);
            Files.createDirectories(pathToFile.getParent());
            if (!Files.exists(pathToFile))
                Files.createFile(pathToFile);

            if (isCompression && LZW_EXTENSION.equals(extension)) {
                LzwZip lzwZip = (LzwZip) selectedAlgo;
                FilesUtils.writeCodesInFile(lzwZip.getCompressedValue(), new FileWriter(fileName));
            } else {
                byte[] zip = selectedAlgo.getResult().readAllBytes();
                ByteArrayOutputStream aZip = new ByteArrayOutputStream();
                aZip.write(zip, 0, zip.length);
                FilesUtils.writeByteArrayOutputStreamInfile(fileName, aZip);
            }
            System.out.println(String.format("%s ended successfully, file available here : '%s'", (isCompression) ? "Compression" : "Decompression", new File(fileName).getAbsolutePath()));
        }
    }

    @SuppressWarnings({"squid:S2189", "InfiniteLoopStatement"})
    public static void main(String[] args) throws IOException {
        BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
        while (true) {
            initVars();
            showOptionsMenu(keyboard);
            showCompressionDecompression(keyboard);
            showAlgorithms(keyboard);
            showExecutionStatus(keyboard);
        }
    }
}
