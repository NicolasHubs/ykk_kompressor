package fr.istic.vv.strategy;

import java.io.IOException;
import java.io.InputStream;

public interface Strategy {

    void configure(InputStream inputStream) throws IOException;
    void execute() throws IOException;
    InputStream getResult() throws IOException;
}
