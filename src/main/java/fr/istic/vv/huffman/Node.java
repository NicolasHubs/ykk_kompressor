package fr.istic.vv.huffman;

public abstract class Node {
    // This constructor is package-private to prevent accidental subclassing outside of this package.
    Node() {}
}
