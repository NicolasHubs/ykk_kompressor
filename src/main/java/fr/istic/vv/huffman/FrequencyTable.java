package fr.istic.vv.huffman;

import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Queue;

public final class FrequencyTable {

    // Length at least 2, and every element is non-negative.
    private int[] frequencies;

    public FrequencyTable(int[] freqs) {
        Objects.requireNonNull(freqs);
        if (freqs.length < 2)
            throw new IllegalArgumentException("At least 2 symbols needed");
        frequencies = freqs.clone();  // Defensive copy
        for (int x : frequencies) {
            if (x < 0)
                throw new IllegalArgumentException("Negative frequency");
        }
    }



    /*---- Basic methods ----*/

    /**
     * Returns the number of symbols in this frequency table. The result is always at least 2.
     * @return the number of symbols in this frequency table
     */
    public int getSymbolLimit() {
        return frequencies.length;
    }


    /**
     * Returns the frequency of the specified symbol in this frequency table. The result is always non-negative.
     * @param symbol the symbol to query
     * @return the frequency of the specified symbol
     * @throws IllegalArgumentException if the symbol is out of range
     */
    public int get(int symbol) {
        checkSymbol(symbol);
        return frequencies[symbol];
    }


    /**
     * Sets the frequency of the specified symbol in this frequency table to the specified value.
     * @param symbol the symbol whose frequency will be modified
     * @param freq the frequency to set it to, which must be non-negative
     * @throws IllegalArgumentException if the symbol is out of range or the frequency is negative
     */
    public void set(int symbol, int freq) {
        checkSymbol(symbol);
        if (freq < 0)
            throw new IllegalArgumentException("Negative frequency");
        frequencies[symbol] = freq;
    }


    /**
     * Increments the frequency of the specified symbol in this frequency table.
     * @param symbol the symbol whose frequency will be incremented
     * @throws IllegalArgumentException if the symbol is out of range
     * @throws IllegalStateException if the symbol already has
     * the maximum allowed frequency of {@code Integer.MAX_VALUE}
     */
    public void increment(int symbol) {
        checkSymbol(symbol);
        if (frequencies[symbol] == Integer.MAX_VALUE)
            throw new IllegalStateException("Maximum frequency reached");
        frequencies[symbol]++;
    }


    // Returns silently if 0 <= symbol < frequencies.length, otherwise throws an exception.
    private void checkSymbol(int symbol) {
        if (symbol < 0 || symbol >= frequencies.length)
            throw new IllegalArgumentException("Symbol out of range");
    }


    /**
     * Returns a string representation of this frequency table,
     * useful for debugging only, and the format is subject to change.
     * @return a string representation of this frequency table
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < frequencies.length; i++)
            sb.append(String.format("%d\t%d%n", i, frequencies[i]));
        return sb.toString();
    }



    /*---- Advanced methods ----*/

    /**
     * Returns a code tree that is optimal for the symbol frequencies in this table.
     * The tree always contains at least 2 leaves (even if they come from symbols with
     * 0 frequency), to avoid degenerate trees. Note that optimal trees are not unique.
     * @return an optimal code tree for this frequency table
     */
    public CodeTree buildCodeTree() {
        // Note that if two nodes have the same frequency, then the tie is broken
        // by which tree contains the lowest symbol. Thus the algorithm has a
        // deterministic output and does not rely on the queue to break ties.
        Queue<NodeWithFrequency> pqueue = new PriorityQueue<>();

        // Add leaves for symbols with non-zero frequency
        for (int i = 0; i < frequencies.length; i++) {
            if (frequencies[i] > 0)
                pqueue.add(new NodeWithFrequency(new Leaf(i), i, frequencies[i]));
        }

        // Pad with zero-frequency symbols until queue has at least 2 items
        for (int i = 0; i < frequencies.length && pqueue.size() < 2; i++) {
            if (frequencies[i] == 0)
                pqueue.add(new NodeWithFrequency(new Leaf(i), i, 0));
        }
        if (pqueue.size() < 2)
            throw new AssertionError();

        // Repeatedly tie together two nodes with the lowest frequency
        while (pqueue.size() > 1) {
            NodeWithFrequency x = pqueue.remove();
            NodeWithFrequency y = pqueue.remove();
            pqueue.add(new NodeWithFrequency(
                    new InternalNode(x.node, y.node),
                    Math.min(x.lowestSymbol, y.lowestSymbol),
                    x.frequency + y.frequency));
        }

        // Return the remaining node
        return new CodeTree((InternalNode)pqueue.remove().node, frequencies.length);
    }


}