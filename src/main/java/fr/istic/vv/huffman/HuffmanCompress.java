package fr.istic.vv.huffman;

import fr.istic.vv.strategy.Strategy;
import fr.istic.vv.utils.BitOutputStream;

import java.io.*;
import java.util.stream.Collectors;

public final class HuffmanCompress implements Strategy {

    private String content;
    private ByteArrayOutputStream compressContent;

    // Returns a frequency table based on the bytes in the given file.
    // Also contains an extra entry for symbol 256, whose frequency is set to 0.
    private static FrequencyTable getFrequencies(String code) throws IOException {
        FrequencyTable freqs = new FrequencyTable(new int[257]);

        try (InputStream input = new ByteArrayInputStream(code.getBytes())) {
            while (true) {
                int b = input.read();
                if (b == -1)
                    break;
                freqs.increment(b);
            }
        }
        return freqs;
    }

    private void writeCodeLengthTable(BitOutputStream out, CanonicalCode canonCode) throws IOException {
        for (int i = 0; i < canonCode.getSymbolLimit(); i++) {
            int val = canonCode.getCodeLength(i);
            // For this file format, we only support codes up to 255 bits long
            if (val >= 256)
                throw new IOException("The code for a symbol is too long");

            // Write value as 8 bits in big endian
            for (int j = 7; j >= 0; j--)
                out.write((val >>> j) & 1);
        }
    }

    private void compress(CodeTree code, InputStream in, BitOutputStream out) throws IOException {
        HuffmanEncoder enc = new HuffmanEncoder(out);
        enc.setCodeTree(code);
        while (true) {
            int b = in.read();
            if (b == -1)
                break;
            enc.write(b);
        }
        enc.write(256);  // EOF
    }

    @Override
    public void configure(InputStream inputStream) {

        this.content = new BufferedReader(new InputStreamReader(inputStream))
                .lines().collect(Collectors.joining("\n"));

        compressContent = new ByteArrayOutputStream();
    }

    @Override
    public void execute() throws IOException {

        // Read input file once to compute symbol frequencies.
        // The resulting generated code is optimal for static Huffman coding and also canonical.
        FrequencyTable freqs = getFrequencies(content);

        freqs.increment(256);  // EOF symbol gets a frequency of 1
        CodeTree code = freqs.buildCodeTree();

        // Replace code tree with canonical one. For each symbol,
        // the code value may change but the code length stays the same.
        CanonicalCode canonCode = new CanonicalCode(code, freqs.getSymbolLimit());
        code = canonCode.toCodeTree();

        // Read input file again, compress with Huffman coding, and write output file
        try (InputStream in = new ByteArrayInputStream(content.getBytes())) {
            try (BitOutputStream out = new BitOutputStream(compressContent)) {
                writeCodeLengthTable(out, canonCode);
                compress(code, in, out);
            }
        }
    }

    @Override
    public InputStream getResult() {
        return new ByteArrayInputStream(compressContent.toByteArray());
    }

    public String getContent() {
        return content;
    }
}