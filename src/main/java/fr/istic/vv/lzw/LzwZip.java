package fr.istic.vv.lzw;

import fr.istic.vv.strategy.Strategy;
import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LzwZip implements Strategy {

    private InputStream inputStream;
    private List<Integer> compressedValue;

    public List<Integer> compress(Charset charset) {
        List<Integer> result = new ArrayList<>();

        if (inputStream != null) {
            int dicoSize = 256;
            String input = new BufferedReader(new InputStreamReader(inputStream, charset))
                    .lines().collect(Collectors.joining("\n"));
            Map<String, Integer> dictionary = new HashMap<>();

            for (int i = 0; i < dicoSize; i++)
                dictionary.put("" + (char) i, i);

            String w = "";
            for (char c : input.toCharArray()) {
                String wc = w + c;
                if (dictionary.containsKey(wc)) {
                    w = wc;
                } else {
                    dictionary.put(wc, dicoSize++);
                    result.add(dictionary.get(w));
                    w = String.valueOf(c);
                }
            }
            if (!w.equals(""))
                result.add(dictionary.get(w));
        }
        return result;
    }

    @Override
    public void configure(InputStream inputStream) {
        this.inputStream = inputStream;
        compressedValue = new ArrayList<>();
    }

    @Override
    public void execute() {
        compressedValue = compress(Charset.defaultCharset());
    }

    public List<Integer> getCompressedValue() {
        return compressedValue;
    }

    @Override
    public InputStream getResult() {
        return null;
    }
}
