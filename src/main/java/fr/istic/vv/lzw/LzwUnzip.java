package fr.istic.vv.lzw;

import fr.istic.vv.strategy.Strategy;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LzwUnzip implements Strategy {

    private InputStream inputStream;
    private String decompressedValue;

    public String decompress(Charset charset) throws IOException {
        StringBuilder result = new StringBuilder();
        if (inputStream != null) {
            List<Integer> compressed = getIntegerList(inputStream, charset);
            if (!compressed.isEmpty()) {
                int dictSize = 256;
                Map<Integer, String> dictionary = new HashMap<>();
                for (int i = 0; i < 256; i++)
                    dictionary.put(i, "" + (char) i);

                String w = "" + (char) (int) compressed.remove(0);
                result.append(w);
                for (int k : compressed) {
                    String entry;
                    if (dictionary.containsKey(k))
                        entry = dictionary.get(k);
                    else if (k == dictSize)
                        entry = w + w.charAt(0);
                    else
                        throw new IllegalArgumentException("Bad compressed k: " + k);

                    result.append(entry);
                    dictionary.put(dictSize++, w + entry.charAt(0));
                    w = entry;
                }
            }
        }
        return result.toString();
    }

    public List<Integer> getIntegerList(InputStream inputStream, Charset charset) throws IOException {
        List<Integer> compressedValues = new ArrayList<>();
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, charset));

        double value;
        // reads to the end of the stream
        while ((value = br.read()) != -1) {
            compressedValues.add((int) value);
        }
        return compressedValues;
    }

    @Override
    public void configure(InputStream inputStream) {
        this.inputStream = inputStream;
        decompressedValue = "";
    }

    @Override
    public void execute() throws IOException {
        decompressedValue = decompress(Charset.defaultCharset());
    }

    @Override
    public InputStream getResult() {
        return new ByteArrayInputStream(decompressedValue.getBytes());
    }
}
