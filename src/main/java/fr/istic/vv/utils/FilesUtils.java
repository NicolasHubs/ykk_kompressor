package fr.istic.vv.utils;

import java.io.*;
import java.util.List;

public class FilesUtils {

    private FilesUtils() {
        // nothing to do here
    }

    /**
     * Write the content of the ByteArrayOuputStream in a File
     *
     * @param outputFilename name of the File to write in
     * @param baos           the content to write
     * @throws IOException - IOException
     */
    public static void writeByteArrayOutputStreamInfile(String outputFilename, ByteArrayOutputStream baos) throws IOException {
        OutputStream outputStream = new FileOutputStream(outputFilename);
        baos.writeTo(outputStream);
    }

    public static void writeCodesInFile(List<Integer> words, FileWriter fileWriter) throws IOException {
        BufferedWriter out = new BufferedWriter(fileWriter);
        for (Integer word : words)
            out.write(word);
        out.flush();
        out.close();
    }
}
